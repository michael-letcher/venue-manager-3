# VenueManager

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.10.

## Navigation

1. `Angular to Angular` -> acts as expected
1. **WARNING** `Angular to AngularJS` -> fallback to iFrame if no valid route is found
   1. Would cause a infinite loop as both frameworks bounce back at each other if neither have a valid route
1. `AngularJS to AngularJS` -> acts as expected, iFrame notifies the parent the update the URL
1. `AngularJS to Angular` -> AngularJS router if a route is not found will fallback to parent iFrame
1. **WARNING** `Angular to unknown` -> Angular to AngularJS
1. **WARNING** `AngularJS to unknown` -> AngularJS to Angular

### Challenges

iFrame must stay alive, this has been decided from feedback and as such is now a technical requirement.
Originally the design has the iFrame destroyed upon navigation. But this would mean access to the AngularJS' services not possible.
It also causes large loading times as each time you navigated into the iFrame would require the legacy VM to do it's initial load.

**Other issues:**

- What happens when AngularJS fails to find a route and notifies Angular, the iFrame should be hidden but what does the AngularJS router do and what state is the legacy app left in?
  - Need to make sure that the fallback route does not trigger unintended side effects.
- How to handle the infinite loop?
  - Need to implement a solution to ensure an infinite loop is broken (`Angular to unknown` / `AngularJS to unknown`) and we route to a 404 page.
- How to convert URLs into state names for the legacy UI-Router
  - When navigating `Angular to AngularJS` we send the route. i.e `/manage/bookings` this does not directly relate to state. So we need to create a solution that can translate URL into a state to transition to.
  - Possible solution is to `hack` the UI router to get access to a states internal config object and loop over all states URL and do our own matching.
    [Hack](https://stackoverflow.com/questions/29892353/angular-ui-router-resolve-state-from-url/30926025#30926025)
  - This could be very hard for the QA testers to automate. Having all of old UI under a iFrame could make automation hard/ unintended side effects.
  - Cross browser support for how we are using iFrames is unknown at this time. We're not too worried about this, but it is something to note.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
