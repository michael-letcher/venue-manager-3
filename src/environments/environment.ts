// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api: 'http://api.roller.local/api', // API URL should include the `/api` folder
  app_api: 'http://roller.local/api', // API URL should include the `/api` folder
  doorlist: 'http://doorlist.roller.local',
  cdn: '//staging-cdn.rollerdigital.com',
  connect: '',
  logging: '',
  auth: {
    clientId: 'fi8yb2dNsPhBoZfxcENb8xfrnE740dlB',
    domain: 'roller-dev.au.auth0.com',
    audience: 'http://roller.local'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
