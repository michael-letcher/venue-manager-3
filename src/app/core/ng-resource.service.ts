import { Injectable } from '@angular/core';

/**
 * Create “wrapper” services that grab references to the AngularJS providers and make them available via
 * Angular’s dependency injection system. This allows us to create one class that
 * will maintain the consumption of this legacy provider in one service, and when
 * we are ready to migrate the provider’s logic, we only need to update the code in one place.
 *
 * @example
 * ```typescript
 * export class MyElementComponent {
 *   // Grab reference to an AngularJS service
 *   const weatherProvider = this.ngService.getResource(‘weatherService’);
 *
 *   constructor(private ngService: NgService) {}
 * }
 * ```
 */
@Injectable({ providedIn: 'root' })
export class NgService {
  $injector: any;

  constructor() {
    this.loadInjector();
  }

  private loadInjector() {
    const iframe: HTMLIFrameElement | null = document.getElementById('vmFrame') as HTMLIFrameElement;

    if (iframe) {
      const window = iframe.contentWindow;
      const doc = window?.document;
      console.debug('iframe window', window);

      if (window) {
        const ngElement = doc?.getElementById('ng-app_rollerVenueApp');
        this.$injector = (window as any).angular?.element(ngElement).injector();
      }
    }
  }

  getResource<T = null>(name: string): T | null {
    if (!this.$injector) {
      console.error('Could not find AngularJS Injector');
      return null;
    }

    if (!this.$injector.has(name)) {
      console.error(`Could not find ${name} provider`);
      return null;
    }

    return this.$injector.get(name) as T;
  }
}
