import { Component, OnDestroy } from '@angular/core';
import { IframeService } from '../angularjs/iframe.service';

@Component({
  selector: 'roller-empty',
  templateUrl: './empty.component.html',
  styleUrls: ['./empty.component.scss']
})
export class EmptyComponent implements OnDestroy {
  constructor(private iframeService: IframeService) {
    this.iframeService.showIframe();
  }

  ngOnDestroy(): void {
    this.iframeService.hideIframe();
  }
}
