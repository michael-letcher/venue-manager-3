import { Component } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { IframeService } from './angularjs/iframe.service';

@Component({
  selector: 'roller-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'venue-manager';

  isAuthenticated$ = this.authService.isAuthenticated$;
  user$ = this.authService.user$;
  visible$ = this.iframeService.visible$;

  constructor(private authService: AuthService, private iframeService: IframeService) {}

  onLogin(): void {
    this.authService.loginWithRedirect();
  }

  onLogout(): void {
    this.authService.logout({ returnTo: window.location.origin });
  }

  onHide(): void {
    this.iframeService.hideIframe();
  }
}
