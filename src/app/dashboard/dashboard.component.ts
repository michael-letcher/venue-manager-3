import { Component } from '@angular/core';

@Component({
  selector: 'roller-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  // TODO ngService causes circular DI issues
  // constructor(private ngService: NgService) {
  //   const accountService = this.ngService.getResource('accountService');
  //   console.log('accountService', accountService);
  // }
}
