import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IframeService {
  private visibleSubject = new BehaviorSubject(false);

  visible$ = this.visibleSubject.asObservable();

  hideIframe(): void {
    console.log('HIDE iFrame');
    this.visibleSubject.next(false);
  }

  showIframe(): void {
    console.log('SHOW iFrame');
    this.visibleSubject.next(true);
  }
}
