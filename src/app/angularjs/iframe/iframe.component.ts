import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NavigationEnd, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { filter } from 'rxjs/operators';
import { IframeService } from '../iframe.service';

@Component({
  selector: 'roller-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.scss']
})
export class IframeComponent {
  url: SafeResourceUrl = '';

  constructor(
    private sanitizer: DomSanitizer,
    private router: Router,
    private location: Location,
    private iframeService: IframeService
  ) {
    const urlEvent = this.router.events.pipe(
      filter((event) => {
        return event instanceof NavigationEnd;
      })
    );

    combineLatest([urlEvent, this.iframeService.visible$]).subscribe(([event, visible$]) => {
      console.warn('iFrame tick', event);

      if (visible$) {
        if (!this.url) {
          this.setUrl((event as NavigationEnd).url);
        } else {
          this.getIframe()?.contentWindow?.postMessage(
            {
              navigateTo: (event as NavigationEnd).url
            },
            '*'
          );
        }
      } else {
        console.log('Do not set URL as iFrame is not visible:', (event as NavigationEnd).url);
      }
    });

    this.listenForRoutingEvents();
  }

  private getIframe(): HTMLIFrameElement {
    return document.getElementById('vmFrame') as HTMLIFrameElement;
  }

  /**
   * Listen for both fallback events (navigateTo) or internal routing events (go)
   */
  private listenForRoutingEvents(): void {
    window.addEventListener(
      'message',
      (e: any) => {
        if (e.data.navigateTo) {
          console.log('AngularJS to Angular', e.data);
          // Need to ignore this route call as it's from the initial call
          // TODO does this need to be a once off or all occurrences???
          if (e.data !== '/') {
            // AngularJS didn't find a route, so notifies parent to navigate
            const url = e.data.navigateTo;
            this.iframeService.hideIframe();
            this.router.navigateByUrl(url);
          }
        } else if (e.data.go) {
          // Used to force URL route. eg. to external site
          console.log('location.go', e.data);
          const url = e.data.go;
          this.location.go(url);
        } else if (e.data.navigatedTo) {
          // AngularJS to AngularJS
          console.warn('AngularJS to AngularJS', e.data.navigatedTo);
          this.location.replaceState(e.data.navigatedTo);
        }
      },
      false
    );
  }

  loadEvent(): void {
    this.setSession();
  }

  private setSession(): void {
    const accessTokenKey = 'ls.authenticationService:accessToken';
    const idTokenPayloadKey = 'ls.authenticationService:idTokenPayload';

    const accessToken = localStorage.getItem(accessTokenKey);
    const expiresIn = localStorage.getItem('expiresIn');
    const idTokenPayload = localStorage.getItem(idTokenPayloadKey);
    const idToken = JSON.parse(idTokenPayload || '');

    if (document.getElementById('vmFrame')) {
      (document.getElementById('vmFrame') as any).contentWindow.postMessage(
        JSON.stringify({ accessToken, expiresIn, idToken }),
        '*'
      );
    }
  }

  private setUrl(url: string): void {
    const requestedUrl = (window as any).env?.legacyVm || 'http://localhost:3002' + url;
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(requestedUrl);
    console.log('Set URL', this.url);
  }
}
