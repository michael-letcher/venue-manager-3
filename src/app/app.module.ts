import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AuthModule, AuthService, IdToken } from '@auth0/auth0-angular';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AngularjsModule } from './angularjs/angularjs.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { EmptyComponent } from './empty/empty.component';

@NgModule({
  declarations: [AppComponent, EmptyComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    DashboardModule,
    AngularjsModule,
    // Import the module into the application, with configuration
    AuthModule.forRoot({
      domain: environment.auth.domain,
      clientId: environment.auth.clientId,
      responseType: 'token id_token',
      audience: environment.auth.audience,
      redirectUri: window.location.origin,
      scope: 'openid profile write:payment access:activitystream'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private authService: AuthService) {
    this.authService.error$.subscribe((error) => console.error(error));
    this.authService.idTokenClaims$.pipe(take(1)).subscribe((idToken) => {
      if (!idToken) {
        this.authService.loginWithRedirect();
      } else {
        this.setSession(idToken);
      }
    });
  }

  setSession(idToken: IdToken): void {
    const accessTokenKey = 'ls.authenticationService:accessToken';
    const idTokenKey = 'ls.authenticationService:idToken';
    const expiryKey = 'ls.authenticationService:expiry';
    const idTokenPayloadKey = 'ls.authenticationService:idTokenPayload';
    // Set the time that the Access Token will expire at
    const expiresAt = idToken.expiresIn ? JSON.stringify(idToken.expiresIn * 1000 + new Date().getTime()) : '';

    localStorage.setItem(accessTokenKey, idToken.__raw);
    localStorage.setItem(idTokenKey, idToken.__raw);
    localStorage.setItem(expiryKey, expiresAt);
    localStorage.setItem('expiresIn', idToken.exp?.toString() || '');
    localStorage.setItem(idTokenPayloadKey, JSON.stringify(idToken));
  }
}
